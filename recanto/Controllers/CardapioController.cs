﻿using recanto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace recanto.Controllers
{
    public class CardapioController : Controller
    {
        // GET: Cardapio
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Salvar()
        {
            return View();
        }
        public ActionResult Entrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Salvar(string Nome, string Preco)
        {
            string msg = "";
            Cardapio v = new Cardapio();
            v.Nome = Nome;
            v.Preco = Preco;
            foreach (string nomeArquivo in Request.Files)
            {
                HttpPostedFileBase arquivoPostado = Request.Files[nomeArquivo];
                string extArquivo = arquivoPostado.ContentType;

                if (extArquivo.Contains("png") || extArquivo.Contains("jpeg"))
                {
                    //salvar no BD

                    //pega o tamanho do arquivo
                    int tamanhoArquivo = arquivoPostado.ContentLength;
                    //cria uma estrutura pra guardar todo o arquivo em bytes
                    byte[] bytesArquivo = new byte[tamanhoArquivo];

                    //converter o arquivo para bytes e guardar no array
                    arquivoPostado.InputStream.Read(bytesArquivo, 0, tamanhoArquivo);
                    v.Foto = bytesArquivo;
                    msg = v.Inserir();
                    TempData["msg"] = msg;
                    return RedirectToAction("Cardapio");
                }
            }
            msg = "Erro ao salvar";
            TempData["msg"] = msg;
            return RedirectToAction("Cardapio");
        }

        public ActionResult Excluir(string id)
        {
            Cardapio v = new Cardapio();
            v.Preco = id;

            string msg = v.Excluir();
            TempData["msg"] = msg;
            return RedirectToAction("Cardapio");
        }

        public ActionResult Cardapio()
        {
            List<Cardapio> lista = recanto.Models.Cardapio.listarcardapio();
            return View(lista);
        }

        
    }

}
﻿using recanto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace recanto.Controllers
{
    public class PedidoController : Controller
    {
        // GET: Pedido
        public ActionResult Pedido()
        {
            return View();
        }
        public ActionResult Salvar()
        {
            return View();
        }




        [HttpPost]
        public ActionResult Salvar(string Nome, string Observacao)
        {
            string msg = "";
            Pedido v = new Pedido();
            v.Nome = Nome;
            v.Observacao = Observacao;
            
            msg = "ERRO AO SALVAR";
            TempData["msg"] = msg;
            return RedirectToAction("Pedido");
        }

        public ActionResult Excluir(string id)
        {
            Pedido v = new Pedido();
            v.Observacao = id;

            string msg = v.Excluir();
            TempData["msg"] = msg;
            return RedirectToAction("Pedido");
        }

        public ActionResult Pedidos()
        {
            List<Pedido> lista = recanto.Models.Pedido.listarpedido();
            return View(lista);
        }


    }

}
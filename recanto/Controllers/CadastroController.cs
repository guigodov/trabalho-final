﻿using recanto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace recanto.Controllers
{
    public class CadastroController : Controller
    {
        // GET: Cadastro
        public ActionResult Entrar()
        {
            return View();
        }

        public ActionResult Salvar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Salvar(string Nome, string Idade, string cpf)
        {
            string msg = "";
            Cadastro v = new Cadastro();
            v.Nome = Nome;
            v.Idade = Idade;
            v.Cpf = cpf;
            
            
            msg = "Erro ao salvar";
            TempData["msg"] = msg;
            return RedirectToAction("Cadastro");
        }
           

        

        public ActionResult Cadastro()
        {
            List<Cadastro> lista = recanto.Models.Cadastro.listarcadastro();
            return View(lista);
        }


    }
}
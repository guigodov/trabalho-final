﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace recanto.Models
{
    public class Pedido
    {
        private const string connectionString = "Server=localhost;Database=recantobelo;User Id=root;Password=dovale";
        private string nome, observacao;
      


        public string Nome { get => nome; set => nome = value; }
        public string Observacao { get => observacao; set => observacao = value; }


        public string Inserir()
        {
            MySqlConnection con =
               new MySqlConnection(connectionString);

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("INSERT INTO pedido (nome, observacao) VALUES(@nome,@observacao)", con);
                query.Parameters.AddWithValue("@nome", nome);
                query.Parameters.AddWithValue("@observacao", observacao);
                



                query.ExecuteNonQuery();

                con.Close();

                return "Inserido com sucesso!";
            }
            catch (Exception ex)
            {
                return "ERRO: " + ex.Message;
            }

        }

        public string Excluir()
        {
            MySqlConnection con = new MySqlConnection(connectionString);

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("DELETE FROM pedido WHERE nome = @nome", con);
                query.Parameters.AddWithValue("@nome", nome);

                query.ExecuteNonQuery();

                con.Close();

                return "Excluido com sucesso!";
            }
            catch (Exception ex)
            {
                return "ERRO: " + ex.Message;
            }
        }

        public static List<Pedido> listarpedido()
        {
            MySqlConnection con = new MySqlConnection(connectionString);

            List<Pedido> lista = new List<Pedido>();

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("SELECT * FROM pedido", con);
                MySqlDataReader leitor = query.ExecuteReader();

                //percorrer o resultado
                while (leitor.Read())
                {
                    //criar o item da lista
                    Pedido item = new Pedido();
                    item.nome = leitor["nome"].ToString();
                    item.observacao = leitor["observacao"].ToString();
                    

                    lista.Add(item);
                }

                con.Close();
            }

            catch (Exception ex)
            {
                lista = null;
            }

            return lista;
        }
    }
}
﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace recanto.Models
{
    public class Cardapio
    {
        private const string connectionString = "Server=localhost;Database=recantobelo;User Id=root;Password=dovale";
        private string nome, preco;
         private byte[]   foto;

        public string Nome { get => nome; set => nome = value; }
        public string Preco { get => preco; set => preco = value; }
        public byte[] Foto { get => foto; set => foto = value; }

        public string Inserir()
        {
            MySqlConnection con =
               new MySqlConnection(connectionString);

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("INSERT INTO produtos (nome, valor, foto) VALUES(@nome,@preco,@foto)", con);
                query.Parameters.AddWithValue("@nome", nome);
                query.Parameters.AddWithValue("@preco", preco);
                query.Parameters.AddWithValue("@foto", foto);



                query.ExecuteNonQuery();

                con.Close();

                return "Inserido com sucesso!";
            }
            catch (Exception ex)
            {
                return "ERRO: " + ex.Message;
            }

        }

        public string Excluir()
        {
            MySqlConnection con = new MySqlConnection(connectionString);

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("DELETE FROM produtos WHERE Preco = @preco", con);
                query.Parameters.AddWithValue("@preco", preco);

                query.ExecuteNonQuery();

                con.Close();

                return "Excluido com sucesso!";
            }
            catch (Exception ex)
            {
                return "ERRO: " + ex.Message;
            }
        }

        public static List<Cardapio> listarcardapio()
        {
            MySqlConnection con = new MySqlConnection(connectionString);

            List<Cardapio> lista = new List<Cardapio>();

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("SELECT * FROM produtos", con);
                MySqlDataReader leitor = query.ExecuteReader();

                //percorrer o resultado
                while (leitor.Read())
                {
                    //criar o item da lista
                    Cardapio item = new Cardapio();
                    item.nome = leitor["nome"].ToString();
                    item.preco = leitor["valor"].ToString();
                    item.foto = (byte[])leitor["foto"];

                    lista.Add(item);
                }

                con.Close();
            }

            catch (Exception ex)
            {
                lista = null;
            }

            return lista;
        }
    }
}
﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace recanto.Models
{
    public class Cadastro
    {
        private const string connectionString = "Server=localhost;Database=recantobelo;User Id=root;Password=dovale";
        private string nome, endereco;
        private string idade, cpf;


        public string Nome { get => nome; set => nome = value; }
        public string Endereco { get => endereco; set => endereco = value; }
        public string Idade { get => idade; set => idade = value; }
        public string Cpf { get => cpf; set => cpf = value; }

        public string Inserir()
        {
            MySqlConnection con =
               new MySqlConnection(connectionString);

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("INSERT INTO cadastro (nome, endereco, idade, cpf) VALUES(@nome,@endereco,@idade,@cpf)", con);
                query.Parameters.AddWithValue("@nome", nome);
                query.Parameters.AddWithValue("@endereco", endereco);
                query.Parameters.AddWithValue("@idade", idade);
                query.Parameters.AddWithValue("@cpf", cpf);



                query.ExecuteNonQuery();

                con.Close();

                return "Inserido com sucesso!";
            }
            catch (Exception ex)
            {
                return "ERRO: " + ex.Message;
            }

        }

        

        public static List<Cadastro> listarcadastro()
        {
            MySqlConnection con = new MySqlConnection(connectionString);

            List<Cadastro> lista = new List<Cadastro>();

            try
            {
                con.Open();

                MySqlCommand query = new MySqlCommand("SELECT * FROM cadastro", con);
                MySqlDataReader leitor = query.ExecuteReader();

                //percorrer o resultado
                while (leitor.Read())
                {
                    //criar o item da lista
                    Cadastro item = new Cadastro();
                    item.nome = leitor["nome"].ToString();
                    item.endereco = leitor["endereco"].ToString();
                    item.idade = leitor["idade"].ToString();
                    item.cpf = leitor["cpf"].ToString();

                    lista.Add(item);
                }

                con.Close();
            }

            catch (Exception ex)
            {
                lista = null;
            }

            return lista;
        }
    }
}